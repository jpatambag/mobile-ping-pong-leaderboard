# Mobile Ping Pong Leaderboard

Context and goal

Mobile Ping Pong Leaderboard application using an API provided, to show the stored win/loss records of the top 3 players.

Basic requirements that need to be fulfilled are as follows:

    Build this leaderboard using the latest version of React Native
    Ensure it runs and displays correctly on an Android and iOS flagship smart phone (latest version on the emulators is fine)
    Produce a Readme file (markdown preferred) with basic instructions on how to configure a local development environment and build the project
    Consume the websocket or RESTful services provided by the anow-leaderboard-services project.  This project has been deployed for you already, with endpoints at:
        GET http://ping-pong.anow.com/leaderboard - this will give you the scoreboard sorted by ELO.
        ws://ping-pong.anow.com/leaderboard - this is the web sockets version of the above, which will push scoreboard changes to your client when they change.  This is our preferred way to consume the API.
    Implement the following user stories:
        As a user, I can toggle the rating/sorting of users by raw Elo rating (returned by the API), or by the user's win/loss ratio (you'll need to calculate this). For the latter, in the event of a tie, the person with most wins is the leader. For example, if Stephen has 12 wins and 6 losses, and Dan has 6 wins and 3 losses, they both have a win/loss ratio of 2. However, Stephen is considered the leader because he has more wins)
        As a user I can see the at least the top 3 people.
        As a user, I can see at person's name, Elo rating, wins, losses at a glance (i.e. I don't need to drill down to get these details.)
        As a user, I want the leader board to be pleasing to the eye (we are looking for something that you consider polished enough to put in front of a paying client as a final product).
        As a user, I want the the leaderboard should update in near real-time when the scores change (within 10 seconds)


Running the project

1. Clone the repository.
2. Should have installed latest react native version installed.
3. Open terminal, and go to PingPongLeaderboard folder. 
4. Run npm install.
5. For ios, you need to install the libraries. So go to /ios folder.
6. Run pod install.
7. Once installed, go back to the root folder.
8. Run react-native run-ios or react-native run-android.