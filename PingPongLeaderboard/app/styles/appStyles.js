const React = require('react-native')
import { Dimensions } from "react-native";
const { StyleSheet } = React

const { width } = Dimensions.get('window');

var app_styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    separator: {
        height: 1,
        width: "100%",
        backgroundColor: "#CED0CE",
    },
    h2text: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 36,
        fontWeight: 'bold',
    },
    flatview: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 20,
        borderRadius: 2,
    },
    listContainer: {
        alignItems: 'center',
        backgroundColor: '#FBFBFB'
    },
    rowStyle: {
        flex: 0.09,
        backgroundColor: '#D8D8D8',
        justifyContent: 'center'
    },
    listItem: {
        flexDirection: 'row',
        width: width,
        height: 85,
        backgroundColor: '#FBFBFB',
    },
    playersStats: {
        flex: 0.92,
        paddingLeft: 5,
        flexDirection: 'row'
    },
    styleRankNumber: {
        fontFamily: 'Verdana',
        fontSize: 25,
        textAlign: 'center',
        color: 'black'
    },
    styleRank: {
        fontFamily: 'Verdana',
        fontSize: 25,
        textAlign: 'center',
        color: 'white'
    },
    name: {
        flex: 0.6,
        justifyContent: 'center'
    },
    styleListItem: {
        flex: 0.3,
        justifyContent: 'center'
    },
    styleListItemRight: {
        flex: 0.25,
        justifyContent: 'center'
    },
    nameText: {
        fontFamily: 'Verdana',
        fontSize: 20,
        textAlign: 'center',
        paddingBottom: 18
    },
    listItemWinLoss: {
        fontFamily: 'Verdana',
        fontSize: 16,
        paddingLeft: 1
    },
    listItemWinLossRight: {
        fontFamily: 'Verdana',
        fontSize: 16,
        paddingLeft: 1
    },
    listItemBottom: {
        fontFamily: 'Verdana',
        color: 'grey',
        fontSize: 13,
        opacity: 0.7
    },
    listItemElo: {
        fontFamily: 'Verdana',
        color: 'grey',
        fontSize: 13,
        opacity: 0.7,
        paddingLeft: 3
    }
})

module.exports = app_styles