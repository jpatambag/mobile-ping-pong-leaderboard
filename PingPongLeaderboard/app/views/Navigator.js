import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './Home/Home'

const rootView = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            gesturesEnabled: true,
        },
    },
},
    {
        initialRouteName: 'Home',
        //headerMode: 'none',
    },
);

export default createAppContainer(rootView);