import React, { Component } from 'react';
import { View, Text, FlatList, Button, Icon, Platform, ScrollView, Image } from "react-native";
import appStyles from '../../styles/appStyles'
import Api from '../../services/Api';

import ActionSheet from 'react-native-action-sheet';

var sortKey = 'wins';
var btnActionSheet = [
    'Win/Loss',
    'ELO',
    'Cancel'
];


class Home extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: 'Ping Pong Leaderboard',
            headerTitleStyle: { color: '#fff' },
            headerStyle: { backgroundColor: '#3c3c3c' },
            headerRight: <Button onPress={() => params.handleAction()} title="Sort" color="#fff" />
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            leaderboard: [],
            refresh: false
        };

        this.toggleSortByKey = this.toggleSortByKey.bind(this)
    }

    componentDidMount() {
        this.fetchData()
        this.props.navigation.setParams({ handleAction: this.toggleSortByKey });

        this._interval = setInterval(() => {
            this.fetchData()
        }, 10000);
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    async fetchData() {
        const data = await Api.fetchLeaderboard();
        let sorted = data.sort((a, b) => b.wins - a.wins)
        sorted.map((data, key) => { data.rank = key + 1 })
        this.setState({
            leaderboard: sorted
        });
    }

    toggleSortByKey() {
        ActionSheet.showActionSheetWithOptions({
            options: btnActionSheet,
            cancelButtonIndex: 2,
            destructiveButtonIndex: 2,
            tintColor: 'black'
        },
            (buttonIndex) => {
                console.log('btn index:' + buttonIndex);
                switch (buttonIndex) {
                    case 0:
                        sortKey = "win/loss"
                        this.sortByWinsOverLossRatio()
                        break;
                    case 1:
                        sortKey = "elo"
                        this.sortByElo()
                        break;
                    default:
                        break;
                }
            });
    }

    sortByWinsOverLossRatio() {
        let { leaderboard } = this.state;

        let sorted = leaderboard.sort((a, b) => {
            let _a = a.wins / a.losses;
            let _b = b.wins / b.losses;

            console.log('_a : ' + _a + ' ' + '_b:' + _b);
            console.log('compare _a & _b ' + (_a === _b));

            // if (_a === _b) {
            //     a.wins > b.wins
            // } else {
            //     b.wins - a.wins
            // }
        });
        sorted.map((data, key) => { data.rank = key + 1 })
        this.setState({
            leaderboard: sorted,
            refresh: true,
        })
    }

    sortByWins() {
        let { leaderboard } = this.state;
        let sorted = leaderboard.sort((a, b) => b.wins - a.wins)
        sorted.map((data, key) => { data.rank = key + 1 })
        this.setState({
            leaderboard: sorted,
            refresh: true,
        })
    }

    sortByElo() {
        let { leaderboard } = this.state;
        let sorted = leaderboard.sort((a, b) => b.elo - a.elo)
        sorted.map((data, key) => { data.rank = key + 1 })
        this.setState({
            leaderboard: sorted,
            refresh: true,
        })
    }

    renderSeparator = () => {
        return (
            <View
                style={appStyles.separator}
            />
        );
    };


    addMedalIndicator = (place) => {
        if (place === 1) return '🥇';
        else if (place === 2) return '🥈';
        else if (place === 3) return '🥉';
    }

    render() {
        return (
            <View style={appStyles.container} >
                <FlatList
                    data={this.state.leaderboard}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) =>
                        <View key={item.rank} style={appStyles.listItem}>
                            <View style={appStyles.rowStyle}>
                                <Text style={appStyles.styleRankNumber}>
                                    {this.addMedalIndicator(item.rank)}
                                    {item.rank}
                                </Text>
                            </View>

                            <View style={appStyles.playersStats}>
                                <View style={appStyles.name}>
                                    <Text style={appStyles.nameText}>{item.name}</Text>
                                </View>

                                <View style={appStyles.styleListItem}>
                                    <Text style={appStyles.listItemWinLoss}>
                                        {item.wins}
                                    </Text>
                                    <Text style={appStyles.listItemBottom}>Wins</Text>
                                </View>

                                <View style={appStyles.styleListItem}>
                                    <Text style={appStyles.listItemWinLoss}>
                                        {item.losses}
                                    </Text>
                                    <Text style={appStyles.listItemBottom}>Losses</Text>
                                </View>

                                <View style={appStyles.styleListItemRight}>
                                    <Text style={appStyles.listItemWinLossRight}>
                                        {item.elo}
                                    </Text>
                                    <Text style={appStyles.listItemElo}>ELO</Text>
                                </View>
                            </View>
                        </View>

                    }
                    keyExtractor={item => `${item.name}`}
                    extraData={this.state.refresh}
                    ItemSeparatorComponent={this.renderSeparator}
                />
            </View>
        );
    }
}
export default Home