export default {
    async fetchLeaderboard() {
        try {
            let response = await fetch('http://ping-pong.anow.com/leaderboard');
            let responseJsonData = await response.json();
            return responseJsonData;
        }
        catch (e) {
            console.log(e)
        }
    }
}