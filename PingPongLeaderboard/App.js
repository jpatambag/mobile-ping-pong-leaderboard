/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import { StatusBar, SafeAreaView, View } from 'react-native';
import Navigator from './app/views/Navigator';

class App extends Component {
  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <Navigator />
      </Fragment>
    );
  }
}

export default App
